variable "location" {}
variable "vnet_name" {}
variable "vnet_resource_group" {}
variable "subnet_cidr" {}

resource "azurerm_resource_group" "GitCanary" {
  name     = "GitCanary"
  location = "${var.location}"
}

resource "azurerm_network_security_group" "GitCanary" {
  name                = "GitCanary"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitCanary.name}"
}

resource "azurerm_network_security_rule" "checkmk" {
  name                        = "checkmk"
  priority                    = 152
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.28.130.79/32"
  destination_port_range      = "6556"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.GitCanary.name}"
  network_security_group_name = "${azurerm_network_security_group.GitCanary.name}"
}

resource "azurerm_network_security_rule" "https" {
  name                        = "https"
  priority                    = 145
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "443"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.GitCanary.name}"
  network_security_group_name = "${azurerm_network_security_group.GitCanary.name}"
}

resource "azurerm_network_security_rule" "ssh" {
  name                        = "ssh"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.254.4.0/23"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.GitCanary.name}"
  network_security_group_name = "${azurerm_network_security_group.GitCanary.name}"
}

resource "azurerm_network_security_rule" "prometheus" {
  name                        = "prometheus"
  priority                    = 151
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.4.1.0/24"
  destination_port_range      = "9100"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.GitCanary.name}"
  network_security_group_name = "${azurerm_network_security_group.GitCanary.name}"
}

resource "azurerm_network_security_rule" "http" {
  name                        = "http"
  priority                    = 140
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "80"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.GitCanary.name}"
  network_security_group_name = "${azurerm_network_security_group.GitCanary.name}"
}

resource "azurerm_subnet" "GitCanary" {
  name                      = "GitCanary"
  resource_group_name       = "${var.vnet_resource_group}"
  virtual_network_name      = "${var.vnet_name}"
  address_prefix            = "${var.subnet_cidr}"
  network_security_group_id = "${azurerm_network_security_group.GitCanary.id}"
}

output "subnet_id" {
  value = "${azurerm_subnet.GitCanary.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.GitCanary.address_prefix}"
}

output "resource_group_name" {
  value = "GitCanary"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.GitCanary.id}"
}
