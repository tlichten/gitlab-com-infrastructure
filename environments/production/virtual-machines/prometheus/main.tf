variable "name" {}

variable "node_count" {
  description = "How many to create"
}

variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
  default     = "prometheus"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "gitlab_net_zone_id" {}

variable "node_count_offset" {
  default = 0
}

resource "azurerm_public_ip" "prometheus" {
  count                        = "${var.node_count}"
  name                         = "${format("%v-%02d-gitlab-net-public-ip", var.name, count.index + 1 + var.node_count_offset)}"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "${format("%v-%02d", var.name, count.index + 1 + var.node_count_offset)}"
}

resource "aws_route53_record" "prometheus" {
  count   = "${var.node_count}"
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "${format("%v-%02d.gitlab.net.", var.name, count.index + 1 + var.node_count_offset)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_public_ip.prometheus.*.ip_address[count.index]}"]
}

resource "azurerm_network_interface" "prometheus" {
  count                     = "${var.node_count}"
  name                      = "${format("%v-%02d-gitlab-net", var.name, count.index + 1 + var.node_count_offset)}"
  internal_dns_name_label   = "${format("%v-%02d-gitlab-net", var.name, count.index + 1 + var.node_count_offset)}"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  network_security_group_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/prometheus/providers/Microsoft.Network/networkSecurityGroups/prometheus2nsg338"

  ip_configuration {
    name                          = "${format("%v-%02d-gitlab-net-ip-configuration", var.name, count.index + 1 + var.node_count_offset)}"
    private_ip_address_allocation = "dynamic"
    subnet_id                     = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourcegroups/prometheus/providers/Microsoft.Network/virtualNetworks/Monitoring/subnets/default"
    public_ip_address_id          = "${azurerm_public_ip.prometheus.*.id[count.index]}"
  }
}

resource "azurerm_virtual_machine" "prometheus" {
  count                         = "${var.node_count}"
  name                          = "${format("%v-%02d-gitlab-net", var.name, count.index + 1 + var.node_count_offset)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.prometheus.*.id[count.index]}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-%02d-prometheus",count.index + 1 + var.node_count_offset)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${format("datadisk-%02d-prometheus",count.index + 1 + var.node_count_offset)}"
    managed_disk_type = "Standard_LRS"
    create_option     = "Empty"
    lun               = 0
    disk_size_gb      = "2048"
  }

  os_profile {
    computer_name  = "${format("%v-%02d.gitlab.net", var.name,  count.index + 1 + var.node_count_offset)}"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}
