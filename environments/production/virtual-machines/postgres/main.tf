variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "count" {}
variable "environment" {}
variable "gitlab_com_zone_id" {}
variable "instance_type" {}
variable "location" {}
variable "resource_group_name" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}

resource "azurerm_availability_set" "postgres" {
  name                         = "${format("postgres-%v", var.environment)}"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "postgres" {
  count                   = "${var.count}"
  name                    = "${format("postgres-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  internal_dns_name_label = "${format("postgres-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("postgres-%02d-%v", count.index + 1, var.environment)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${join(".", slice(split(".", var.address_prefix), 0, 3))}.${count.index + 101}"
  }
}

resource "aws_route53_record" "postgres" {
  count   = "${var.count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("postgres-%02d.%v.%v.gitlab.com.", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.postgres.*.private_ip_address[count.index]}"]
}

resource "azurerm_managed_disk" "postgres-datadisk-0" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-0", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-1" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-1", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-2" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-2", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-3" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-3", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-4" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-4", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-5" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-5", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-6" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-6", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-7" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-7", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-8" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-8", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

resource "azurerm_managed_disk" "postgres-datadisk-9" {
  count                = "${var.count}"
  name                 = "${format("postgres-%02d-prd-datadisk-9", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "512"
}

data "template_file" "chef-bootstrap-postgres" {
  count    = "${var.count}"
  template = "${file("${path.root}/../../templates/chef-bootstrap-ssh-keys.tpl")}"

  vars {
    chef_repo_dir   = "${var.chef_repo_dir}"
    chef_vaults     = "${var.chef_vaults}"
    chef_vault_env  = "${var.chef_vault_env}"
    chef_version    = "${var.chef_version}"
    environment     = "${var.environment}"
    hostname        = "${format("postgres-%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    ip_address      = "${azurerm_network_interface.postgres.*.private_ip_address[count.index]}"
    ssh_private_key = "${var.ssh_private_key}"
    ssh_user        = "${var.ssh_user}"
  }
}

resource "azurerm_virtual_machine" "postgres" {
  count                         = "${var.count}"
  name                          = "${format("postgres-%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.postgres.id}"
  network_interface_ids         = ["${azurerm_network_interface.postgres.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.postgres.*.id[count.index]}"
  vm_size                       = "${var.instance_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-postgres-%02d-%v", count.index + 1, var.environment)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-0.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-0.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-0.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-1.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-1.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-1.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-2.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-2.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-2.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-3.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-3.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-3.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-4.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-4.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-4.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-5.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-5.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-5.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-6.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-6.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-6.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-7.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-7.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-7.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-8.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-8.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-8.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.postgres-datadisk-9.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.postgres-datadisk-9.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.postgres-datadisk-9.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  os_profile {
    computer_name  = "${format("postgres-%02d.%v.%v.gitlab.com", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    admin_username = "${var.ssh_user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys = {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_public_key}")}"
    }
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-postgres.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type        = "ssh"
      host        = "${azurerm_network_interface.postgres.*.private_ip_address[count.index]}"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_private_key}")}"
      timeout     = "10s"
    }
  }
}
