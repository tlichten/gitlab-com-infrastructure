output "altssh_backend_pool_id" {
  value = "${azurerm_lb_backend_address_pool.AltSSHLBProd.id}"
}

output "frontend_backend_pool_id" {
  value = "${azurerm_lb_backend_address_pool.FrontEndLBProd.id}"
}

output "pages_backend_pool_id" {
  value = "${azurerm_lb_backend_address_pool.PagesLBProd.id}"
}
