variable "location" {}

variable "address_space" {
  type = "list"
}

resource "azurerm_resource_group" "GitLabProd" {
  name     = "GitLabProd"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "GitLabProd" {
  name                = "GitLabProd"
  address_space       = "${var.address_space}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabProd.name}"
}

output "id" {
  value = "${azurerm_virtual_network.GitLabProd.id}"
}

output "name" {
  value = "${azurerm_virtual_network.GitLabProd.name}"
}

output "resource_group_name" {
  value = "${azurerm_resource_group.GitLabProd.name}"
}
