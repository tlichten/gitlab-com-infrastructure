variable "environment" {
  default = "geo"
}

variable "arm_subscription_id" {}
variable "arm_client_id" {}
variable "arm_client_secret" {}
variable "arm_tenant_id" {}
variable "chef_repo_dir" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

variable "first_user_username" {}
variable "first_user_password" {}

variable "backup_aws_access_key" {}
variable "backup_aws_secret_key" {}
variable "disk_subscription" {}
variable "disk_snapshot" {}

variable "azure_location" {
  default = "East US 2"
}

variable "gce_location" {
  default = "us-east1-b"
}

variable "gce_machine_type" {
  default = "n1-standard-8"
}

variable "gce_name" {
  default = "geo-sync"
}

variable "google_credentials" {}
variable "google_project" {}

variable "vpn_ips" {
  default = ["52.177.194.133", "52.177.192.239"]
}

variable "outbound_deny" {
  default = true
}

variable "ssh_user" {
  default = "terraform"
}

variable "ssh_key_path" {
  default = ""
}

provider "google" {
  credentials = "${var.google_credentials}"
  project     = "${var.google_project}"
  region      = "${var.gce_location}"
}

provider "azurerm" {
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_client_id}"
  client_secret   = "${var.arm_client_secret}"
  tenant_id       = "${var.arm_tenant_id}"
}

provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {}
}

## Azure Resource Group for GEO

resource "azurerm_resource_group" "GeoTestbed" {
  name     = "GeoTestbed"
  location = "${var.azure_location}"
}

module "gitlab-restore-gce-nfs" {
  count                 = 2
  source                = "../../modules/gitlab-restore-gce-nfs"
  location              = "${var.gce_location}"
  name                  = "sync-nfs"
  disk_size             = "${55 * 1024}"
  backup_aws_access_key = "${var.backup_aws_access_key}"
  backup_aws_secret_key = "${var.backup_aws_secret_key}"
  prod_ip               = "${module.gitlab-restore-single.public_ip}"
  vpn_ips               = "${var.vpn_ips}"
}

module "gitlab-single-gce" {
  source                = "../../modules/gitlab-single-gce"
  location              = "${var.gce_location}"
  machine_type          = "${var.gce_machine_type}"
  name                  = "${var.gce_name}"
  disk_size             = "${20 * 1024}"
  backup_aws_access_key = "${var.backup_aws_access_key}"
  backup_aws_secret_key = "${var.backup_aws_secret_key}"
  prod_ip               = "${module.gitlab-restore-single.public_ip}"
  vpn_ips               = "${var.vpn_ips}"
}

module "gitlab-azure-security" {
  source              = "../../modules/gitlab-azure-security"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  outbound_deny       = "${var.outbound_deny}"
}

module "gitlab-restore-single" {
  source                = "../../modules/gitlab-restore-single"
  disk_snapshot_date    = "2017-08-09"
  restore_machine       = "file-08"
  location              = "${var.azure_location}"
  resource_group_name   = "${azurerm_resource_group.GeoTestbed.name}"
  first_user_username   = "${var.first_user_username}"
  first_user_password   = "${var.first_user_password}"
  backup_aws_access_key = "${var.backup_aws_access_key}"
  backup_aws_secret_key = "${var.backup_aws_secret_key}"
  disk_subscription     = "${var.disk_subscription}"
  security_group_id     = "${module.gitlab-azure-security.id}"
  ssh_key_path          = "${path.root}/../../private/ssh/geo-nfs-uploads"
  ssh_user              = "terraform"
}

resource "aws_route53_record" "prod" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "prod.geo.gitlab.com."
  type    = "A"
  ttl     = "300"
  records = ["${module.gitlab-restore-single.public_ip}"]
}

resource "aws_route53_record" "sync" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "sync.geo.gitlab.com."
  type    = "A"
  ttl     = "300"
  records = ["${module.gitlab-single-gce.public_ip}"]
}

module "gitlab-disk-restore-azure-uploads" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-uploads"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "uploads-01"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-lfs-01" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-lfs"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "lfs-01"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-01" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-01"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-01"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-02" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-02"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-02"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-03" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-03"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-03"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-04" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-04"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-04"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-05" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-05"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-05"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-06" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-06"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-06"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-07" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-07"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-07"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-09" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-09"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-09"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-10" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-10"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-10"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-11" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-11"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-11"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}

module "gitlab-disk-restore-azure-nfs-12" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = true
  name                = "geo-nfs-12"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "file-12"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.GeoTestbed.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 16
}
