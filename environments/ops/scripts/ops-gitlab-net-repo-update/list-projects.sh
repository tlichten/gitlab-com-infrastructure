#!/usr/bin/env bash

#  2807845 for security-tools
for group in 2807845 gitlab-cookbooks gl-infra; do

    pages=$(curl -sI "https://gitlab.com/api/v4/groups/$group/projects?private_token=$GITLAB_PRIVATE_TOKEN" | perl -ne 'm/X-Total-Pages: (\d+)/ && print $1')

    for i in $(seq 1 "$pages"); do
        for p in $(curl -s "https://gitlab.com/api/v4/groups/$group/projects?private_token=$GITLAB_PRIVATE_TOKEN&page=$i" | jq -r '.[] | .path_with_namespace'); do
            echo ${p}
        done
    done
done

# Add other paths that we want to mirror but without grabbing the entire group

echo "gitlab-com/influxdb-relay"
echo "gitlab-com/runbooks"
