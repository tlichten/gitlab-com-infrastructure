This script is for populating repositories on the ops instance
it creates pull mirrors for every project, and populates CICD vars

How to use:

1. Create ops-instance.env from ops-instance.env.example
1. Copy it to the ops instance
1. Create a list of repo paths that you want to sync, use the `list-projects.sh`
   script and drop the file in /tmp/projects.list
1. Run `update-ops-gitlab-net-projects.rb` on the rails console by running
   `gitlab-rails runner update-ops-gitlab-net-projects.rb`
