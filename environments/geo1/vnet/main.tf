variable "location" {
  description = "The location"
}

variable "virtual_network_cidr" {
  description = "The CIDR of the virtual network"
}

resource "azurerm_resource_group" "GitLabGeo1" {
  name     = "GitLabGeo1"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "GitLabGeo1" {
  name                = "GitLabGeo1"
  address_space       = ["${var.virtual_network_cidr}"]
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabGeo1.name}"
}

resource "azurerm_network_security_group" "gitlab-geo1-nsg" {
  name                = "gitlab-geo1-nsg"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabGeo1.name}"

  security_rule {
    name                       = "default-allow-ssh"
    priority                   = 1000
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "TCP"
    source_port_range          = "*"
    source_address_prefix      = "*"
    destination_port_range     = "22"
    destination_address_prefix = "*"
  }
}

output "id" {
  value = "${azurerm_virtual_network.GitLabGeo1.id}"
}

output "name" {
  value = "${azurerm_virtual_network.GitLabGeo1.name}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.gitlab-geo1-nsg.id}"
}

output "resource_group_name" {
  value = "${azurerm_resource_group.GitLabGeo1.name}"
}

output "virtual_network_cidr" {
  value = "${var.virtual_network_cidr}"
}
