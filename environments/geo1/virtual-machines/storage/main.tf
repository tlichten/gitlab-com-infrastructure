variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "gitlab_com_zone_id" {}
variable "environment" {}
variable "tier" {}

variable "disk_names_file08" {
  default = []
}

variable "disk_ids_file08" {
  default = []
}

variable "disk_sizes_file08" {
  default = []
}

variable "disk_names_uploads" {
  default = []
}

variable "disk_ids_uploads" {
  default = []
}

variable "disk_sizes_uploads" {
  default = []
}

variable "disk_names_lfs" {
  default = []
}

variable "disk_ids_lfs" {
  default = []
}

variable "disk_sizes_lfs" {
  default = []
}

resource "azurerm_availability_set" "StorageGeo1" {
  name                         = "StorageGeo1"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.StorageGeo1.id}"
}

### geo1-nfs-file-08
resource "azurerm_public_ip" "geo1-nfs-file-08-public-ip" {
  name                         = "geo1-nfs-file-08-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "geo1-nfs-file-08"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "geo1-nfs-file-08" {
  name                    = "geo1-nfs-file-08"
  internal_dns_name_label = "geo1-nfs-file-08"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "geo1-nfs-file-08-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.205.2.108"
    public_ip_address_id          = "${azurerm_public_ip.geo1-nfs-file-08-public-ip.id}"
  }
}

resource "aws_route53_record" "geo1-nfs-file-08" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("nfs-file-08.%v.%v.gitlab.com.", var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.geo1-nfs-file-08.private_ip_address}"]
}

resource "azurerm_virtual_machine" "geo1-nfs-file-08" {
  name                          = "geo1-nfs-file-08"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.geo1-nfs-file-08.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-geo1-nfs-file-08"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "geo1-nfs-file-08"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[0]}"
    managed_disk_id = "${var.disk_ids_file08[0]}"
    disk_size_gb    = "${var.disk_sizes_file08[0]}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[1]}"
    managed_disk_id = "${var.disk_ids_file08[1]}"
    disk_size_gb    = "${var.disk_sizes_file08[1]}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[2]}"
    managed_disk_id = "${var.disk_ids_file08[2]}"
    disk_size_gb    = "${var.disk_sizes_file08[2]}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[3]}"
    managed_disk_id = "${var.disk_ids_file08[3]}"
    disk_size_gb    = "${var.disk_sizes_file08[3]}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[4]}"
    managed_disk_id = "${var.disk_ids_file08[4]}"
    disk_size_gb    = "${var.disk_sizes_file08[4]}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[5]}"
    managed_disk_id = "${var.disk_ids_file08[5]}"
    disk_size_gb    = "${var.disk_sizes_file08[5]}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[6]}"
    managed_disk_id = "${var.disk_ids_file08[6]}"
    disk_size_gb    = "${var.disk_sizes_file08[6]}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[7]}"
    managed_disk_id = "${var.disk_ids_file08[7]}"
    disk_size_gb    = "${var.disk_sizes_file08[7]}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[8]}"
    managed_disk_id = "${var.disk_ids_file08[8]}"
    disk_size_gb    = "${var.disk_sizes_file08[8]}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[9]}"
    managed_disk_id = "${var.disk_ids_file08[9]}"
    disk_size_gb    = "${var.disk_sizes_file08[9]}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[10]}"
    managed_disk_id = "${var.disk_ids_file08[10]}"
    disk_size_gb    = "${var.disk_sizes_file08[10]}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[11]}"
    managed_disk_id = "${var.disk_ids_file08[11]}"
    disk_size_gb    = "${var.disk_sizes_file08[11]}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[12]}"
    managed_disk_id = "${var.disk_ids_file08[12]}"
    disk_size_gb    = "${var.disk_sizes_file08[12]}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[13]}"
    managed_disk_id = "${var.disk_ids_file08[13]}"
    disk_size_gb    = "${var.disk_sizes_file08[13]}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[14]}"
    managed_disk_id = "${var.disk_ids_file08[14]}"
    disk_size_gb    = "${var.disk_sizes_file08[14]}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_file08[15]}"
    managed_disk_id = "${var.disk_ids_file08[15]}"
    disk_size_gb    = "${var.disk_sizes_file08[15]}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### geo1-nfs-uploads-01
resource "azurerm_public_ip" "geo1-nfs-uploads-01-public-ip" {
  name                         = "geo1-nfs-uploads-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "geo1-nfs-uploads-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "geo1-nfs-uploads-01" {
  name                    = "geo1-nfs-uploads-01"
  internal_dns_name_label = "geo1-nfs-uploads-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "geo1-nfs-uploads-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.205.2.141"
    public_ip_address_id          = "${azurerm_public_ip.geo1-nfs-uploads-01-public-ip.id}"
  }
}

resource "aws_route53_record" "geo1-nfs-uploads-01" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("nfs-uploads-01.%v.%v.gitlab.com.", var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.geo1-nfs-uploads-01.private_ip_address}"]
}

resource "azurerm_virtual_machine" "geo1-nfs-uploads-01" {
  name                          = "geo1-nfs-uploads-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.geo1-nfs-uploads-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-geo1-nfs-uploads-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "geo1-nfs-uploads-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[0]}"
    managed_disk_id = "${var.disk_ids_uploads[0]}"
    disk_size_gb    = "${var.disk_sizes_uploads[0]}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[1]}"
    managed_disk_id = "${var.disk_ids_uploads[1]}"
    disk_size_gb    = "${var.disk_sizes_uploads[1]}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[2]}"
    managed_disk_id = "${var.disk_ids_uploads[2]}"
    disk_size_gb    = "${var.disk_sizes_uploads[2]}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[3]}"
    managed_disk_id = "${var.disk_ids_uploads[3]}"
    disk_size_gb    = "${var.disk_sizes_uploads[3]}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[4]}"
    managed_disk_id = "${var.disk_ids_uploads[4]}"
    disk_size_gb    = "${var.disk_sizes_uploads[4]}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[5]}"
    managed_disk_id = "${var.disk_ids_uploads[5]}"
    disk_size_gb    = "${var.disk_sizes_uploads[5]}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[6]}"
    managed_disk_id = "${var.disk_ids_uploads[6]}"
    disk_size_gb    = "${var.disk_sizes_uploads[6]}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[7]}"
    managed_disk_id = "${var.disk_ids_uploads[7]}"
    disk_size_gb    = "${var.disk_sizes_uploads[7]}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[8]}"
    managed_disk_id = "${var.disk_ids_uploads[8]}"
    disk_size_gb    = "${var.disk_sizes_uploads[8]}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[9]}"
    managed_disk_id = "${var.disk_ids_uploads[9]}"
    disk_size_gb    = "${var.disk_sizes_uploads[9]}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[10]}"
    managed_disk_id = "${var.disk_ids_uploads[10]}"
    disk_size_gb    = "${var.disk_sizes_uploads[10]}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[11]}"
    managed_disk_id = "${var.disk_ids_uploads[11]}"
    disk_size_gb    = "${var.disk_sizes_uploads[11]}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[12]}"
    managed_disk_id = "${var.disk_ids_uploads[12]}"
    disk_size_gb    = "${var.disk_sizes_uploads[12]}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[13]}"
    managed_disk_id = "${var.disk_ids_uploads[13]}"
    disk_size_gb    = "${var.disk_sizes_uploads[13]}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[14]}"
    managed_disk_id = "${var.disk_ids_uploads[14]}"
    disk_size_gb    = "${var.disk_sizes_uploads[14]}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_uploads[15]}"
    managed_disk_id = "${var.disk_ids_uploads[15]}"
    disk_size_gb    = "${var.disk_sizes_uploads[15]}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### geo1-nfs-lfs-01
resource "azurerm_public_ip" "geo1-nfs-lfs-01-public-ip" {
  name                         = "geo1-nfs-lfs-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "geo1-nfs-lfs-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "geo1-nfs-lfs-01" {
  name                    = "geo1-nfs-lfs-01"
  internal_dns_name_label = "geo1-nfs-lfs-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "geo1-nfs-lfs-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.205.2.121"
    public_ip_address_id          = "${azurerm_public_ip.geo1-nfs-lfs-01-public-ip.id}"
  }
}

resource "aws_route53_record" "geo1-nfs-lfs-01" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("nfs-lfs-01.%v.%v.gitlab.com.", var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.geo1-nfs-lfs-01.private_ip_address}"]
}

resource "azurerm_virtual_machine" "geo1-nfs-lfs-01" {
  name                          = "geo1-nfs-lfs-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.geo1-nfs-lfs-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-geo1-nfs-lfs-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "geo1-nfs-lfs-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[0]}"
    managed_disk_id = "${var.disk_ids_lfs[0]}"
    disk_size_gb    = "${var.disk_sizes_lfs[0]}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[1]}"
    managed_disk_id = "${var.disk_ids_lfs[1]}"
    disk_size_gb    = "${var.disk_sizes_lfs[1]}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[2]}"
    managed_disk_id = "${var.disk_ids_lfs[2]}"
    disk_size_gb    = "${var.disk_sizes_lfs[2]}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[3]}"
    managed_disk_id = "${var.disk_ids_lfs[3]}"
    disk_size_gb    = "${var.disk_sizes_lfs[3]}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[4]}"
    managed_disk_id = "${var.disk_ids_lfs[4]}"
    disk_size_gb    = "${var.disk_sizes_lfs[4]}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[5]}"
    managed_disk_id = "${var.disk_ids_lfs[5]}"
    disk_size_gb    = "${var.disk_sizes_lfs[5]}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[6]}"
    managed_disk_id = "${var.disk_ids_lfs[6]}"
    disk_size_gb    = "${var.disk_sizes_lfs[6]}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[7]}"
    managed_disk_id = "${var.disk_ids_lfs[7]}"
    disk_size_gb    = "${var.disk_sizes_lfs[7]}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[8]}"
    managed_disk_id = "${var.disk_ids_lfs[8]}"
    disk_size_gb    = "${var.disk_sizes_lfs[8]}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[9]}"
    managed_disk_id = "${var.disk_ids_lfs[9]}"
    disk_size_gb    = "${var.disk_sizes_lfs[9]}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[10]}"
    managed_disk_id = "${var.disk_ids_lfs[10]}"
    disk_size_gb    = "${var.disk_sizes_lfs[10]}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[11]}"
    managed_disk_id = "${var.disk_ids_lfs[11]}"
    disk_size_gb    = "${var.disk_sizes_lfs[11]}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[12]}"
    managed_disk_id = "${var.disk_ids_lfs[12]}"
    disk_size_gb    = "${var.disk_sizes_lfs[12]}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[13]}"
    managed_disk_id = "${var.disk_ids_lfs[13]}"
    disk_size_gb    = "${var.disk_sizes_lfs[13]}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[14]}"
    managed_disk_id = "${var.disk_ids_lfs[14]}"
    disk_size_gb    = "${var.disk_sizes_lfs[14]}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${var.disk_names_lfs[15]}"
    managed_disk_id = "${var.disk_ids_lfs[15]}"
    disk_size_gb    = "${var.disk_sizes_lfs[15]}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}
