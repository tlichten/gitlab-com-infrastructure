variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "gitlab_com_zone_id" {}
variable "environment" {}
variable "tier" {}

variable "disk_names" {
  default = []
}

variable "disk_ids" {
  default = []
}

variable "disk_sizes" {
  default = []
}

resource "azurerm_availability_set" "StorageGeo2" {
  name                         = "StorageGeo2"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.StorageGeo2.id}"
}

### geo2-nfs-file-08
resource "azurerm_public_ip" "geo2-nfs-file-08-public-ip" {
  name                         = "geo2-nfs-file-08-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "geo2-nfs-file-08"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "geo2-nfs-file-08" {
  name                    = "geo2-nfs-file-08"
  internal_dns_name_label = "geo2-nfs-file-08"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "geo2-nfs-file-08-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.213.2.108"
    public_ip_address_id          = "${azurerm_public_ip.geo2-nfs-file-08-public-ip.id}"
  }
}

resource "aws_route53_record" "geo2-nfs-file-08" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("nfs-file-08.%v.%v.gitlab.com.", var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.geo2-nfs-file-08.private_ip_address}"]
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-0" {
  name                 = "geo2-file-08-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-1" {
  name                 = "geo2-file-08-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-2" {
  name                 = "geo2-file-08-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-3" {
  name                 = "geo2-file-08-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-4" {
  name                 = "geo2-file-08-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-5" {
  name                 = "geo2-file-08-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-6" {
  name                 = "geo2-file-08-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-7" {
  name                 = "geo2-file-08-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-8" {
  name                 = "geo2-file-08-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-9" {
  name                 = "geo2-file-08-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-10" {
  name                 = "geo2-file-08-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-11" {
  name                 = "geo2-file-08-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-12" {
  name                 = "geo2-file-08-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-13" {
  name                 = "geo2-file-08-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-14" {
  name                 = "geo2-file-08-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-file-08-datadisk-15" {
  name                 = "geo2-file-08-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "geo2-nfs-file-08" {
  name                          = "geo2-nfs-file-08"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.geo2-nfs-file-08.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-geo2-nfs-file-08"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "geo2-nfs-file-08"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-file-08-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-file-08-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-file-08-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### geo2-nfs-uploads-01
resource "azurerm_public_ip" "geo2-nfs-uploads-01-public-ip" {
  name                         = "geo2-nfs-uploads-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "geo2-nfs-uploads-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "geo2-nfs-uploads-01" {
  name                    = "geo2-nfs-uploads-01"
  internal_dns_name_label = "geo2-nfs-uploads-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "geo2-nfs-uploads-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.213.2.141"
    public_ip_address_id          = "${azurerm_public_ip.geo2-nfs-uploads-01-public-ip.id}"
  }
}

resource "aws_route53_record" "geo2-nfs-uploads-01" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("nfs-uploads-01.%v.%v.gitlab.com.", var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.geo2-nfs-uploads-01.private_ip_address}"]
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-0" {
  name                 = "geo2-uploads-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-1" {
  name                 = "geo2-uploads-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-2" {
  name                 = "geo2-uploads-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-3" {
  name                 = "geo2-uploads-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-4" {
  name                 = "geo2-uploads-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-5" {
  name                 = "geo2-uploads-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-6" {
  name                 = "geo2-uploads-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-7" {
  name                 = "geo2-uploads-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-8" {
  name                 = "geo2-uploads-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-9" {
  name                 = "geo2-uploads-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-10" {
  name                 = "geo2-uploads-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-11" {
  name                 = "geo2-uploads-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-12" {
  name                 = "geo2-uploads-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-13" {
  name                 = "geo2-uploads-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-14" {
  name                 = "geo2-uploads-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-uploads-01-datadisk-15" {
  name                 = "geo2-uploads-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "geo2-nfs-uploads-01" {
  name                          = "geo2-nfs-uploads-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.geo2-nfs-uploads-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-geo2-nfs-uploads-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "geo2-nfs-uploads-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-uploads-01-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-uploads-01-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-uploads-01-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### geo2-nfs-lfs-01
resource "azurerm_public_ip" "geo2-nfs-lfs-01-public-ip" {
  name                         = "geo2-nfs-lfs-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "geo2-nfs-lfs-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "geo2-nfs-lfs-01" {
  name                    = "geo2-nfs-lfs-01"
  internal_dns_name_label = "geo2-nfs-lfs-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "geo2-nfs-lfs-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.213.2.121"
    public_ip_address_id          = "${azurerm_public_ip.geo2-nfs-lfs-01-public-ip.id}"
  }
}

resource "aws_route53_record" "geo2-nfs-lfs-01" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("nfs-lfs-01.%v.%v.gitlab.com.", var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.geo2-nfs-lfs-01.private_ip_address}"]
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-0" {
  name                 = "geo2-lfs-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-1" {
  name                 = "geo2-lfs-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-2" {
  name                 = "geo2-lfs-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-3" {
  name                 = "geo2-lfs-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-4" {
  name                 = "geo2-lfs-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-5" {
  name                 = "geo2-lfs-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-6" {
  name                 = "geo2-lfs-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-7" {
  name                 = "geo2-lfs-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-8" {
  name                 = "geo2-lfs-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-9" {
  name                 = "geo2-lfs-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-10" {
  name                 = "geo2-lfs-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-11" {
  name                 = "geo2-lfs-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-12" {
  name                 = "geo2-lfs-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-13" {
  name                 = "geo2-lfs-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-14" {
  name                 = "geo2-lfs-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "geo2-lfs-01-datadisk-15" {
  name                 = "geo2-lfs-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "geo2-nfs-lfs-01" {
  name                          = "geo2-nfs-lfs-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.geo2-nfs-lfs-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-geo2-nfs-lfs-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "geo2-nfs-lfs-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.geo2-lfs-01-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.geo2-lfs-01-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.geo2-lfs-01-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}
