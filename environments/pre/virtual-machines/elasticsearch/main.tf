variable "location" {}
variable "count" {}
variable "resource_group_name" {}
variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "gitlab_com_zone_id" {}

resource "azurerm_availability_set" "ElasticSearchPre" {
  name                         = "ElasticSearchPre"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_public_ip" "elasticsearch" {
  count                        = "${var.count}"
  name                         = "${format("elasticsearch%02d-pre-public-ip", count.index + 1)}"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "${format("elasticsearch%02d-pre", count.index + 1)}"
}

resource "azurerm_network_interface" "elasticsearch" {
  count                   = "${var.count}"
  name                    = "${format("elasticsearch%02d-pre", count.index + 1)}"
  internal_dns_name_label = "${format("elasticsearch%02d-pre", count.index + 1)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("elasticsearch%02d-pre", count.index + 1)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.161.3.${format("%02d", count.index + 101)}"
    public_ip_address_id          = "${azurerm_public_ip.elasticsearch.*.id[count.index]}"
  }
}

resource "aws_route53_record" "elasticsearch" {
  count   = "${var.count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("elasticsearch%02d.pre.gitlab.com.", count.index + 1)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.elasticsearch.*.fqdn[count.index]}."]
}

data "template_file" "chef-bootstrap-elasticsearch" {
  count    = "${var.count}"
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.elasticsearch.*.ip_address[count.index]}"
    hostname            = "${format("elasticsearch%02d.pre.gitlab.com", count.index + 1)}"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

#resource "azurerm_managed_disk" "elasticsearch-datadisk-0" {
#  count                = "${var.count}"
#  name                 = "${format("elasticsearch%02d-pre-datadisk-0", count.index + 1)}"
#  location             = "East US 2"
#  resource_group_name  = "${var.resource_group_name}"
#  storage_account_type = "Premium_LRS"
#  create_option        = "Empty"
#  disk_size_gb         = "1023"
#}
#resource "azurerm_managed_disk" "elasticsearch-datadisk-1" {
#  count                = "${var.count}"
#  name                 = "${format("elasticsearch%02d-pre-datadisk-1", count.index + 1)}"
#  location             = "East US 2"
#  resource_group_name  = "${var.resource_group_name}"
#  storage_account_type = "Premium_LRS"
#  create_option        = "Empty"
#  disk_size_gb         = "1023"
#}
#resource "azurerm_managed_disk" "elasticsearch-datadisk-2" {
#  count                = "${var.count}"
#  name                 = "${format("elasticsearch%02d-pre-datadisk-2", count.index + 1)}"
#  location             = "East US 2"
#  resource_group_name  = "${var.resource_group_name}"
#  storage_account_type = "Premium_LRS"
#  create_option        = "Empty"
#  disk_size_gb         = "1023"
#}
#resource "azurerm_managed_disk" "elasticsearch-datadisk-3" {
#  count                = "${var.count}"
#  name                 = "${format("elasticsearch%02d-pre-datadisk-3", count.index + 1)}"
#  location             = "East US 2"
#  resource_group_name  = "${var.resource_group_name}"
#  storage_account_type = "Premium_LRS"
#  create_option        = "Empty"
#  disk_size_gb         = "1023"
#}

resource "azurerm_virtual_machine" "elasticsearch" {
  count                         = "${var.count}"
  name                          = "${format("elasticsearch%02d.pre.gitlab.com", count.index + 1)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.ElasticSearchPre.id}"
  network_interface_ids         = ["${azurerm_network_interface.elasticsearch.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.elasticsearch.*.id[count.index]}"
  vm_size                       = "Standard_F2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-elasticsearch%02d-pre", count.index + 1)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  #  storage_data_disk {
  #    name          = "${azurerm_managed_disk.elasticsearch-datadisk-0.*.name[count.index]}"
  #    managed_disk_id = "${azurerm_managed_disk.elasticsearch-datadisk-0.*.id[count.index]}"
  #    disk_size_gb  = "${azurerm_managed_disk.elasticsearch-datadisk-0.*.disk_size_gb[count.index]}"
  #    create_option = "Attach"
  #    lun           = 0
  #  }
  #
  #  storage_data_disk {
  #    name          = "${azurerm_managed_disk.elasticsearch-datadisk-1.*.name[count.index]}"
  #    managed_disk_id = "${azurerm_managed_disk.elasticsearch-datadisk-1.*.id[count.index]}"
  #    disk_size_gb  = "${azurerm_managed_disk.elasticsearch-datadisk-1.*.disk_size_gb[count.index]}"
  #    create_option = "Attach"
  #    lun           = 1
  #  }
  #
  #  storage_data_disk {
  #    name          = "${azurerm_managed_disk.elasticsearch-datadisk-2.*.name[count.index]}"
  #    managed_disk_id = "${azurerm_managed_disk.elasticsearch-datadisk-2.*.id[count.index]}"
  #    disk_size_gb  = "${azurerm_managed_disk.elasticsearch-datadisk-2.*.disk_size_gb[count.index]}"
  #    create_option = "Attach"
  #    lun           = 2
  #  }
  #
  #  storage_data_disk {
  #    name          = "${azurerm_managed_disk.elasticsearch-datadisk-3.*.name[count.index]}"
  #    managed_disk_id = "${azurerm_managed_disk.elasticsearch-datadisk-3.*.id[count.index]}"
  #    disk_size_gb  = "${azurerm_managed_disk.elasticsearch-datadisk-3.*.disk_size_gb[count.index]}"
  #    create_option = "Attach"
  #    lun           = 3
  #  }

  os_profile {
    computer_name  = "${format("elasticsearch%02d.pre.gitlab.com", count.index + 1)}"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-elasticsearch.*.rendered[count.index]}"
  }
  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.elasticsearch.*.ip_address[count.index]}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "10s"
    }
  }
}
