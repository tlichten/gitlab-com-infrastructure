variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "gitlab_com_zone_id" {}

resource "azurerm_availability_set" "DeployStaging" {
  name                         = "DeployStaging"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.DeployStaging.id}"
}

resource "azurerm_public_ip" "deploy" {
  name                         = "deploy-stg-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "deploy-stg"
}

resource "azurerm_network_interface" "deploy" {
  name                    = "deploy-stg"
  internal_dns_name_label = "deploy-stg"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "deploy-stg-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.130.3.101"
    public_ip_address_id          = "${azurerm_public_ip.deploy.id}"
  }
}

resource "aws_route53_record" "deploy" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "deploy.stg.gitlab.com"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.deploy.fqdn}."]
}

data "template_file" "bootstrap-deploy" {
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.deploy.ip_address}"
    hostname            = "deploy.stg.gitlab.com"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "deploy" {
  name                          = "deploy.stg.gitlab.com"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.DeployStaging.id}"
  network_interface_ids         = ["${azurerm_network_interface.deploy.id}"]
  vm_size                       = "Standard_A2_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-deploy-stg"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "deploy.stg.gitlab.com"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.bootstrap-deploy.rendered}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.deploy.ip_address}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "30s"
    }
  }
}
