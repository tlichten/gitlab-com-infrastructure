variable "location" {}
variable "vnet_name" {}
variable "vnet_resource_group" {}
variable "subnet_cidr" {}

variable "vault_clients" {
  description = "CIDR, permitted to access Vault (tcp/8200)"
}

resource "azurerm_resource_group" "VaultStaging" {
  name     = "VaultStaging"
  location = "${var.location}"
}

resource "azurerm_network_security_group" "VaultStaging" {
  name                = "VaultStaging"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.VaultStaging.name}"
}

resource "azurerm_network_security_rule" "vault" {
  name                        = "vault"
  description                 = "All of staging (${var.vault_clients}) to staging-vault (${azurerm_subnet.VaultStaging.address_prefix}), tcp/8200"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "${var.vault_clients}"
  destination_port_range      = "8200"
  destination_address_prefix  = "${azurerm_subnet.VaultStaging.address_prefix}"
  resource_group_name         = "${azurerm_resource_group.VaultStaging.name}"
  network_security_group_name = "${azurerm_network_security_group.VaultStaging.name}"
}

resource "azurerm_network_security_rule" "prometheus" {
  name                        = "prometheus"
  description                 = "Prometheus (10.4.1.0/24) to staging-vault (${azurerm_subnet.VaultStaging.address_prefix}), tcp/9100"
  priority                    = 151
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.4.1.0/24"
  destination_port_range      = "9100"
  destination_address_prefix  = "${azurerm_subnet.VaultStaging.address_prefix}"
  resource_group_name         = "${azurerm_resource_group.VaultStaging.name}"
  network_security_group_name = "${azurerm_network_security_group.VaultStaging.name}"
}

resource "azurerm_subnet" "VaultStaging" {
  name                      = "VaultStaging"
  resource_group_name       = "${var.vnet_resource_group}"
  virtual_network_name      = "${var.vnet_name}"
  address_prefix            = "${var.subnet_cidr}"
  network_security_group_id = "${azurerm_network_security_group.VaultStaging.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.VaultStaging.address_prefix}"
}

output "subnet_id" {
  value = "${azurerm_subnet.VaultStaging.id}"
}

output "resource_group_name" {
  value = "VaultStaging"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.VaultStaging.id}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.VaultStaging.id}"
}
