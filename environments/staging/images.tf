variable "images" {
  type = "map"

  default = {
    api = "api-stg-2017-08-25_11-20-19"
    git = "git-stg-2017-08-25_12-08-27"
    web = "web-stg-2017-08-25_15-29-07"
  }
}
