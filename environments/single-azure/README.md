# Summary

This is an environment that can be used for single-server
installs of GitLab in Azure.

# Creating a new single server

* Add the helper scripts to your PATH in the repo `bin/`.
* Run `tf workspace list` to list existing workspaces.
* Come up with a new name for the testbed, hostname compliant and create it

# Create a new single server with or without using a prod db copy

```
cd ~/workspace/gitlab-com-infrastructure/environments/single-azure/

# Pick a name that is not already in use, if unsure `tf workspace list`
# All resources will be placed in single-azure-${workspace} in azure

workspace=NAME

# for snapshots, you need gnu `date` for this to select the previous day
# Add this to your private/env_vars so it is always set if you want:
export TF_VAR_disk_snapshot=$(gdate +%Y-%m-%d -d "1 day ago")

tf workspace new $workspace

# Generate a unique keypair for the workspace
tf apply -target module.gitlab-single-azure.null_resource.pre_keypair

# Pick one the following options:

  # To use production db snapshots
  # This will attach the disks as part of the terraform run
  tf apply -var 'outbound_deny=false' -var 'use_prod_db_snapshots=true'

  # To use a blank db without any extra disks
  tf apply -var 'outbound_deny=false'

  # To use a standard omnibus install with lfs disks from production
  # You will need to attach them manually
  tf apply -var 'outbound_deny=false' -var 'use_lfs_01_snapshots=true'

  # To use a standard omnibus install with upload disks from production
  # You will need to attach them manually
  tf apply -var 'outbound_deny=false' -var 'use_uploads_snapshots=true'

  # To use a standard omnibus install with a single file server disks
  * from production snapshots
  # You will need to attach them manually
  tf apply -var 'outbound_deny=false' -var 'use_file_snapshots=true'


ssh-add ../../private/ssh/terraform-single-azure-$workspace
cd ~/workspace/chef-repo

knife bootstrap $workspace.single.gitlab.com --sudo --node-name $workspace.single.gitlab.com --bootstrap-version "12.19.36" -x terraform
bundle exec rake "add_node_secrets[$workspace.single.gitlab.com, syslog_client]"
bundle exec rake "add_node_secrets[$workspace.single.gitlab.com, single-azure-base]"
bundle exec rake "node_add_role[$workspace.single.gitlab.com, single-azure-base]"

# Optional - If you want this machine to serve https
# Add external url to gitlab_rb
#   "omnibus-gitlab": {
#     "gitlab_rb": {
#       "external_url": "https://$workspace.single.gitlab.com"
#     }
#   }
bundle exec rake "edit_node[$workspace.single.gitlab.com]"

ssh terraform@$workspace.single.gitlab.com 'sudo chef-client'

# To disable outbound network access
cd ~/workspace/gitlab-com-infrastructure/environments/single-azure/
tf apply -target module.gitlab-azure-security.azurerm_network_security_group.single

```
