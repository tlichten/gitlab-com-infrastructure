variable "environment" {
  default = "single-azure"
}

variable "arm_subscription_id" {}
variable "arm_client_id" {}
variable "arm_client_secret" {}
variable "arm_tenant_id" {}
variable "disk_subscription" {}
variable "chef_repo_dir" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

variable "first_user_username" {}
variable "first_user_password" {}

variable "backup_aws_access_key" {}
variable "backup_aws_secret_key" {}

variable "disk_snapshot" {
  # example: "2017-09-12"
}

variable "outbound_deny" {
  default = true
}

variable "use_prod_db_snapshots" {
  default = false
}

variable "use_lfs_01_snapshots" {
  default = false
}

variable "use_uploads_snapshots" {
  default = false
}

variable "use_file_snapshots" {
  default = false
}

variable "azure_location" {
  default = "East US 2"
}

variable "file_host" {
  default = "nfs-08"
}

# These are the new variables to connect to the newly created instance, which
# replace the two above. Note that terraform does not support
# interpolation for default values so we use local values to pass these
# to the modules

variable "ssh_user" {
  default = "terraform"
}

variable "ssh_key_path" {
  default = ""
}

# In the next terraform release we can use locals,
# in 0.10.3 they are broken
# locals {
#   ssh_key = "${data.null_data_source.ssh.inputs["key_path"] != "" ? data.null_data_source.ssh.inputs["key_path"] : "${path.root}/private/ssh/terraform_rsa"}"
#   ssh_user = "${var.ssh_user}"
# }
data "null_data_source" "ssh" {
  inputs = {
    key_path = "${var.ssh_key_path != "" ? var.ssh_key_path : "${path.root}/../../private/ssh/${var.ssh_user}-${var.environment}-${terraform.workspace}"}"
    user     = "${var.ssh_user}"
  }
}

######################

provider "azurerm" {
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_client_id}"
  client_secret   = "${var.arm_client_secret}"
  tenant_id       = "${var.arm_tenant_id}"
}

provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {}

  config {
    workspace_key_prefix = "terraform/single-azure/envs/"
  }
}

## Azure Resource Group for this testbed

resource "azurerm_resource_group" "single" {
  name     = "${var.environment}-${terraform.workspace}"
  location = "${var.azure_location}"
}

module "gitlab-azure-security" {
  source              = "../../modules/gitlab-azure-security"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.single.name}"
  outbound_deny       = "${var.outbound_deny}"
}

module "gitlab-single-azure" {
  name                  = "${terraform.workspace}"
  enable_module         = "${var.use_prod_db_snapshots ? false : true}"
  environment           = "${var.environment}"
  source                = "../../modules/gitlab-single-azure"
  location              = "${var.azure_location}"
  resource_group_name   = "${azurerm_resource_group.single.name}"
  backup_aws_access_key = "${var.backup_aws_access_key}"
  backup_aws_secret_key = "${var.backup_aws_secret_key}"
  ssh_key_path          = "${data.null_data_source.ssh.inputs["key_path"]}"
  ssh_user              = "${data.null_data_source.ssh.inputs["user"]}"
  security_group_id     = "${module.gitlab-azure-security.id}"
  zone_id               = "${var.gitlab_com_zone_id}"
}

module "gitlab-single-azure-db" {
  name                  = "${terraform.workspace}"
  enable_module         = "${var.use_prod_db_snapshots}"
  environment           = "${var.environment}"
  source                = "../../modules/gitlab-single-azure-db"
  location              = "${var.azure_location}"
  resource_group_name   = "${azurerm_resource_group.single.name}"
  backup_aws_access_key = "${var.backup_aws_access_key}"
  backup_aws_secret_key = "${var.backup_aws_secret_key}"
  ssh_key_path          = "${data.null_data_source.ssh.inputs["key_path"]}"
  ssh_user              = "${data.null_data_source.ssh.inputs["user"]}"
  security_group_id     = "${module.gitlab-azure-security.id}"
  disk_names            = "${module.gitlab-disk-restore-azure.names}"
  disk_ids              = "${module.gitlab-disk-restore-azure.ids}"
  disk_sizes            = "${module.gitlab-disk-restore-azure.sizes}"
  zone_id               = "${var.gitlab_com_zone_id}"
}

module "gitlab-disk-restore-azure" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = "${var.use_prod_db_snapshots}"
  name                = "${terraform.workspace}"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "db4"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.single.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 10
}

module "gitlab-disk-restore-azure-uploads" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = "${var.use_uploads_snapshots}"
  name                = "${terraform.workspace}"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "uploads-01"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.single.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 15
}

module "gitlab-disk-restore-azure-lfs-01" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = "${var.use_lfs_01_snapshots}"
  name                = "${terraform.workspace}"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "lfs-01"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.single.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 15
}

module "gitlab-disk-restore-azure-file" {
  source              = "../../modules/gitlab-disk-restore-azure"
  enable_module       = "${var.use_file_snapshots}"
  name                = "${terraform.workspace}"
  disk_snapshot_date  = "${var.disk_snapshot}"
  restore_machine     = "${var.file_host}"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.single.name}"
  disk_subscription   = "${var.disk_subscription}"
  count               = 15
}
