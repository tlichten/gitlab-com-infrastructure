set -eu

cd ${chef_repo_dir}

bundle exec knife bootstrap ${first_user_username}@${ip_address} -P ${first_user_password} --no-host-key-verify --sudo --node-name ${hostname} --bootstrap-version "12.19.36" -r 'role[gitlab]' -j {\"azure\":{\"ipaddress\":\"${ip_address}\"}} -y

for vault in ${chef_vaults}
do
  bundle exec rake 'add_node_secrets[${hostname}, '$vault']'
done

bundle exec knife node from file nodes/${hostname}.json
