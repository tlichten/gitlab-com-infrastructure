# Makefile for installing various utilities during CI
# Copyright 2017
# Licence MIT
# Maintainer Ilya Frolov <ilya@gitlab.com>

# Variables
UNAME		 := $(shell uname -s)
KEY_SERVERS	 := pool.sks-keyservers.net \
	subkeys.pgp.net \
	pgp.mit.edu \
	keyserver.ubuntu.com \
	keys.gnupg.net
#
TF_URL		 := https://releases.hashicorp.com/terraform
HASHICORP_KEY	 := 0x51852D87348FFC4C
# These can be overriden, for ex: with either `TF_ARCH=i386 make tfinstall` or `make TF_ARCH=i386 tfinstall`
TF_VERSION	 ?= 0.9.11
TF_DISTRO	 ?= linux
TF_ARCH		 ?= amd64
TF_INSTALL_TO	 ?= /

# These are just for readability
TF_ZIP		 := terraform_$(TF_VERSION)_$(TF_DISTRO)_$(TF_ARCH).zip
TF_SHA256	 := terraform_$(TF_VERSION)_SHA256SUMS
TF_SHA256SIG	 := terraform_$(TF_VERSION)_SHA256SUMS.sig

TF_URL_ZIP	 := $(TF_URL)/$(TF_VERSION)/$(TF_ZIP)
TF_URL_SHA256	 := $(TF_URL)/$(TF_VERSION)/$(TF_SHA256)
TF_URL_SHA256SIG := $(TF_URL)/$(TF_VERSION)/$(TF_SHA256SIG)

# Vault
VA_URL		 := https://releases.hashicorp.com/vault
# These can be overriden with either `VA_ARCH=i386 make vainstall` or `make VA_ARCH=i386 vainstall`
VA_VERSION	 ?= 0.7.3
VA_DISTRO	 ?= linux
VA_ARCH		 ?= amd64
VA_INSTALL_TO	 ?= /

# These are just for readability
VA_ZIP		 := vault_$(VA_VERSION)_$(VA_DISTRO)_$(VA_ARCH).zip
VA_SHA256	 := vault_$(VA_VERSION)_SHA256SUMS
VA_SHA256SIG	 := vault_$(VA_VERSION)_SHA256SUMS.sig

VA_URL_ZIP	 := $(VA_URL)/$(VA_VERSION)/$(VA_ZIP)
VA_URL_SHA256	 := $(VA_URL)/$(VA_VERSION)/$(VA_SHA256)
VA_URL_SHA256SIG := $(VA_URL)/$(VA_VERSION)/$(VA_SHA256SIG)

# Packer
PK_URL		 := https://releases.hashicorp.com/packer
# These can be overriden with either `PK_ARCH=i386 make pkinstall` or `make PK_ARCH=i386 pkinstall`
PK_VERSION	 ?= 1.0.3
PK_DISTRO	 ?= linux
PK_ARCH		 ?= amd64
PK_INSTALL_TO	 ?= /

# These are just for readability
PK_ZIP		 := packer_$(PK_VERSION)_$(PK_DISTRO)_$(PK_ARCH).zip
PK_SHA256	 := packer_$(PK_VERSION)_SHA256SUMS
PK_SHA256SIG	 := packer_$(PK_VERSION)_SHA256SUMS.sig

PK_URL_ZIP	 := $(PK_URL)/$(PK_VERSION)/$(PK_ZIP)
PK_URL_SHA256	 := $(PK_URL)/$(PK_VERSION)/$(PK_SHA256)
PK_URL_SHA256SIG := $(PK_URL)/$(PK_VERSION)/$(PK_SHA256SIG)

# Consul
CO_URL		 := https://releases.hashicorp.com/consul
# These can be overriden with either `CO_ARCH=i386 make coinstall` or `make CO_ARCH=i386 coinstall`
CO_VERSION	 ?= 0.9.0
CO_DISTRO	 ?= linux
CO_ARCH		 ?= amd64
CO_INSTALL_TO	 ?= /

# These are just for readability
CO_ZIP		 := consul_$(CO_VERSION)_$(CO_DISTRO)_$(CO_ARCH).zip
CO_SHA256	 := consul_$(CO_VERSION)_SHA256SUMS
CO_SHA256SIG	 := consul_$(CO_VERSION)_SHA256SUMS.sig

CO_URL_ZIP	 := $(CO_URL)/$(CO_VERSION)/$(CO_ZIP)
CO_URL_SHA256	 := $(CO_URL)/$(CO_VERSION)/$(CO_SHA256)
CO_URL_SHA256SIG := $(CO_URL)/$(CO_VERSION)/$(CO_SHA256SIG)

# DO ctl
DO_URL		 := https://github.com/digitalocean/doctl/releases/download
# These can be overriden with either `DO_ARCH=i386 make doinstall` or `make DO_ARCH=i386 doinstall`
DO_VERSION	 ?= 1.7.0
DO_DISTRO	 ?= linux
DO_ARCH		 ?= amd64
DO_INSTALL_TO	 ?= /

# These are just for readability
DO_TGZ		 := doctl-$(DO_VERSION)-$(DO_DISTRO)-$(DO_ARCH).tar.gz
DO_SHA256	 := doctl-$(DO_VERSION)-$(DO_DISTRO)-$(DO_ARCH).sha256
DO_URL_TGZ	 := $(DO_URL)/v$(DO_VERSION)/$(DO_TGZ)
DO_URL_SHA256	 := $(DO_URL)/v$(DO_VERSION)/$(DO_SHA256)

# this is godly
# https://news.ycombinator.com/item?id=11939200
.PHONY: help
help:	### This screen. Keep it first target to be default
ifeq ($(UNAME), Linux)
	@grep -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
else
	@# this is not tested, but prepared in advance for you, Mac drivers
	@awk -F ':.*###' '$$0 ~ FS {printf "%15s%s\n", $$1 ":", $$2}' $(MAKEFILE_LIST) | grep -v '@awk' | sort
endif

# Targets
#
.PHONY: debug
debug:	### Debug Makefile itself placeholder
	@echo $(UNAME)

.PHONY: gpgkey
gpgkey:		### Get Hashicop's gpg key from list of servers
	@gpg --list-keys $(HASHICORP_KEY); \
	if [ $$? -eq 0 ]; then \
		echo "Key $(HASHICORP_KEY) is already in keystore"; \
	else \
		for ksrv in $(KEY_SERVERS); do \
			echo -n "Getting key $(HASHICORP_KEY) from server $$ksrv ... "; \
			gpg --keyserver $$ksrv --recv-keys $(HASHICORP_KEY); \
			if [ $$? -eq 0 ]; then \
				echo "Success!"; \
				exit 0 ; \
			else \
				echo "Fail"; \
			fi; \
		done; \
	fi

.PHONY: tfinstall
tfinstall:	### Download, check sum and unpack specific terraform version
tfinstall: gpgkey
	@# First, we download into temporary dir
	$(eval $@_TMP := $(shell mktemp -d "/tmp/tfinstall.tmp.XXXXXX"))
	test -n "$($@_TMP)" || exit 1
	wget --quiet --continue --directory-prefix "$($@_TMP)" \
		"$(TF_URL_ZIP)" \
		"$(TF_URL_SHA256)" \
		"$(TF_URL_SHA256SIG)"
	@# Then, we verify signature on hashsums
	gpg --verbose --verify "$($@_TMP)/$(TF_SHA256SIG)" "$($@_TMP)/$(TF_SHA256)"
	@# Then, we verify hashsum on our zip archive, using only its line as stdin
	cd $($@_TMP) && grep "$(TF_ZIP)" "$(TF_SHA256)" | sha256sum -c -w
	@# Finally, we are confident file is legitimate
	unzip -o "$($@_TMP)/$(TF_ZIP)" -d "$(TF_INSTALL_TO)"
	@# Cleanup
	rm -rf "$($@_TMP)"

.PHONY: vainstall
vainstall:	### Download, check sum and unpack specific vault version
vainstall: gpgkey
	@# First, we download into temporary dir
	$(eval $@_TMP := $(shell mktemp -d "/tmp/vainstall.tmp.XXXXXX"))
	test -n "$($@_TMP)" || exit 1
	wget --quiet --continue --directory-prefix "$($@_TMP)" \
		"$(VA_URL_ZIP)" \
		"$(VA_URL_SHA256)" \
		"$(VA_URL_SHA256SIG)"
	@# Then, we verify signature on hashsums
	gpg --verbose --verify "$($@_TMP)/$(VA_SHA256SIG)" "$($@_TMP)/$(VA_SHA256)"
	@# Then, we verify hashsum on our zip archive, using only its line as stdin
	cd $($@_TMP) && grep "$(VA_ZIP)" "$(VA_SHA256)" | sha256sum -c -w
	@# Finally, we are confident file is legitimate
	unzip -o "$($@_TMP)/$(VA_ZIP)" -d "$(VA_INSTALL_TO)"
	@# Cleanup
	rm -rf "$($@_TMP)"

.PHONY: pkinstall
pkinstall:	### Download, check sum and unpack specific packer version
pkinstall: gpgkey
	@# First, we download into temporary dir
	$(eval $@_TMP := $(shell mktemp -d "/tmp/pkinstall.tmp.XXXXXX"))
	test -n "$($@_TMP)" || exit 1
	wget --quiet --continue --directory-prefix "$($@_TMP)" \
		"$(PK_URL_ZIP)" \
		"$(PK_URL_SHA256)" \
		"$(PK_URL_SHA256SIG)"
	@# Then, we verify signature on hashsums
	gpg --verbose --verify "$($@_TMP)/$(PK_SHA256SIG)" "$($@_TMP)/$(PK_SHA256)"
	@# Then, we verify hashsum on our zip archive, using only its line as stdin
	cd $($@_TMP) && grep "$(PK_ZIP)" "$(PK_SHA256)" | sha256sum -c -w
	@# Finally, we are confident file is legitimate
	unzip -o "$($@_TMP)/$(PK_ZIP)" -d "$(PK_INSTALL_TO)"
	@# Cleanup
	rm -rf "$($@_TMP)"

.PHONY: coinstall
coinstall:	### Download, check sum and unpack specific consul version
coinstall: gpgkey
	@# First, we download into temporary dir
	$(eval $@_TMP := $(shell mktemp -d "/tmp/coinstall.tmp.XXXXXX"))
	test -n "$($@_TMP)" || exit 1
	wget --quiet --continue --directory-prefix "$($@_TMP)" \
		"$(CO_URL_ZIP)" \
		"$(CO_URL_SHA256)" \
		"$(CO_URL_SHA256SIG)"
	@# Then, we verify signature on hashsums
	gpg --verbose --verify "$($@_TMP)/$(CO_SHA256SIG)" "$($@_TMP)/$(CO_SHA256)"
	@# Then, we verify hashsum on our zip archive, using only its line as stdin
	cd $($@_TMP) && grep "$(CO_ZIP)" "$(CO_SHA256)" | sha256sum -c -w
	@# Finally, we are confident file is legitimate
	unzip -o "$($@_TMP)/$(CO_ZIP)" -d "$(CO_INSTALL_TO)"
	@# Cleanup
	rm -rf "$($@_TMP)"

.PHONY: doinstall
doinstall:	### Download, check sum and unpack specific doctl version
	@# First, we download into temporary dir
	$(eval $@_TMP := $(shell mktemp -d "/tmp/doinstall.tmp.XXXXXX"))
	test -n "$($@_TMP)" || exit 1
	wget --continue --directory-prefix "$($@_TMP)" \
		"$(DO_URL_TGZ)" \
		"$(DO_URL_SHA256)"
	@# Then, unpack and verify sum
	cd $($@_TMP) && tar zxf $(DO_TGZ) && cat $(DO_SHA256) | sha256sum -c -w
	@# Finally, install file if it is legitimate
	mv $($@_TMP)/doctl $(DO_INSTALL_TO)/doctl.real
	@# Cleanup
	rm -rf "$($@_TMP)"

.PHONY: tfmt
tfmt:
	@find . -name "*.tf" | xargs -I{} terraform fmt {}
