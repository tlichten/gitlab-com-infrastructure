### Object Storage Configuration

variable "versioning" {
  type    = "string"
  default = "true"
}

variable "artifact_age" {
  type    = "string"
  default = "30"
}

variable "upload_age" {
  type    = "string"
  default = "30"
}

variable "lfs_object_age" {
  type    = "string"
  default = "30"
}

variable "storage_class" {
  type    = "string"
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = "string"
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = "string"
  default = "cloud-storage-analytics@google.com"
}
