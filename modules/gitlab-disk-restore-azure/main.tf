variable "name" {}
variable "disk_snapshot_date" {}
variable "enable_module" {}

# restore machine: db4 / file-01 / file-02 /etc
variable "restore_machine" {}

variable "location" {}
variable "resource_group_name" {}
variable "disk_subscription" {}
variable "count" {}

resource "azurerm_managed_disk" "single" {
  count                = "${var.enable_module ? var.count : 0}"
  name                 = "disk-${var.name}-${count.index}"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Standard_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-${count.index}-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

output "names" {
  value = ["${azurerm_managed_disk.single.*.name}"]
}

output "ids" {
  value = ["${azurerm_managed_disk.single.*.id}"]
}

output "sizes" {
  value = ["${azurerm_managed_disk.single.*.disk_size_gb}"]
}
