#!/bin/bash
set -ex
exec &> >(tee -a "/tmp/bootstrap.log")

useradd -u 1100 git # this uid is used for the restore
export DEBIAN_FRONTEND=noninteractive

mkdir -p /var/opt/gitlab
mkfs.ext4 -F /dev/disk/by-id/google-gitlab_var
mount /dev/disk/by-id/google-gitlab_var /var/opt/gitlab

# install everything we need for nfs
apt-get -y install nfs-kernel-server

echo '/var/opt/gitlab  10.0.0.0/8(rw,sync,no_subtree_check,no_root_squash)' >> /etc/exports
service nfs-kernel-server restart
