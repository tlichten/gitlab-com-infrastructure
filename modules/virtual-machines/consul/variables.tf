variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "environment" {}
variable "first_user_password" {}
variable "first_user_username" {}
variable "gitlab_net_zone_id" {}
variable "location" {}
variable "resource_group_name" {}
variable "count" {}
variable "instance_type" {}
variable "subnet_id" {}
variable "tier" {}
