##################################
#
#  Pubsub topics for logging
#  one per pubsub host
#
##################################

resource "google_pubsub_topic" "mytopic" {
  count = "${length(var.names)}"
  name  = "${format("%v-%v-%v-%v", var.name, var.names[count.index], var.tier, var.environment)}"
}
