output "instances_self_link" {
  value = "${google_compute_instance.default.*.self_link}"
}

output "instance_groups_self_link" {
  value = "${google_compute_instance_group.default.*.self_link}"
}

output "instance_public_ips" {
  value = "${google_compute_instance.default.*.network_interface.0.access_config.0.assigned_nat_ip}"
}
