variable "service_ports" {
  type    = "list"
  default = ["443", "80"]
}

variable "web_ip_fqdn" {}
variable "backend_service_link" {}

variable "cert_link" {
  type        = "string"
  description = "resource link for the ssl certificate"
}

variable "gitlab_zone_id" {
  type        = "string"
  description = "Zone id for creating dns records (AWS)"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "name" {
  type        = "string"
  description = "The pet name"
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "zone" {
  type    = "string"
  default = ""
}
