variable "url_map" {}

variable "hosts" {
  type = "list"
}

variable "service_ports" {
  type        = "list"
  description = "ports to allow for healthchecks"
}

variable "subnetwork_name" {
  type        = "string"
  description = "subnetwork name for the instances"
}

variable "cert_link" {
  type        = "string"
  description = "resource link for the ssl certificate"
}

variable "gitlab_net_zone_id" {
  type        = "string"
  description = "Zone id for creating dns records (AWS)"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "name" {
  type        = "string"
  description = "The pet name"
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "zone" {
  type    = "string"
  default = ""
}

variable "targets" {
  type        = "list"
  description = "target tags for the load balancer"
}
