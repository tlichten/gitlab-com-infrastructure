variable "health_check_port" {
  default = "use service port"
}

variable "use_external_ip" {
  default = false
}

variable "backend_service_type" {
  default     = "regular"
  description = "type of backend service, either normal or regional"
}

variable "kernel_version" {
  default = ""
}

variable "use_new_node_name" {
  default = false
}

variable "default_scopes" {
  type = "list"

  default = [
    "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
    "https://www.googleapis.com/auth/pubsub",
    "https://www.googleapis.com/auth/service.management.readonly",
    "https://www.googleapis.com/auth/servicecontrol",
    "https://www.googleapis.com/auth/trace.append",
    "https://www.googleapis.com/auth/cloudkms",
    "https://www.googleapis.com/auth/compute.readonly",
  ]

  description = "default permission scopes."
}

variable "additional_scopes" {
  type        = "list"
  default     = []
  description = "Additional permission scopes."
}

variable "create_backend_service" {
  default = true
}

variable "enable_iap" {
  default = false
}

variable "oauth2_client_id" {
  default = ""
}

variable "oauth2_client_secret" {
  default = ""
}

variable "allow_stopping_for_update" {
  description = "wether Terraform is allowed to stop the instance to update its properties"
  default     = "false"
}

variable "backend_protocol" {
  default = "HTTP"
}

variable "health_check" {
  default = "http"
}

variable "service_port" {
  type    = "string"
  default = "80"
}

variable "service_path" {
  default = "/"
}

variable "subnetwork_name" {
  type        = "string"
  default     = ""
  description = "subnetwork name for the instances"
}

variable "block_project_ssh_keys" {
  type        = "string"
  description = "Whether to block project level SSH keys"
  default     = "TRUE"
}

variable "bootstrap_version" {
  description = "version of the bootstrap script"
  default     = 1
}

variable "chef_init_run_list" {
  type        = "string"
  default     = ""
  description = "run_list for the node in chef that are ran on the first boot only"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = "string"
  description = "run_list for the node in chef"
}

variable "dns_zone_name" {
  type        = "string"
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "enable_oslogin" {
  type        = "string"
  description = "Whether to enable OS Login GCP feature"

  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = "FALSE"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "log_disk_size" {
  type        = "string"
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = "string"
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "ip_cidr_range" {
  type        = "string"
  description = "The IP range"
  default     = "169.254.0.1/32"
}

variable "machine_type" {
  type        = "string"
  description = "The machine size"
}

variable "name" {
  type        = "string"
  description = "The pet name"
}

variable "node_count" {
  type        = "string"
  description = "The nodes count"
}

variable "os_boot_image" {
  type        = "string"
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-1604-xenial-v20180122"
}

variable "os_disk_size" {
  type        = "string"
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = "string"
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = "string"
  description = "Use preemptible instances for this pet"
  default     = "false"
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "public_ports" {
  type        = "list"
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "service_account_email" {
  type        = "string"
  description = "Service account emails under which the instance is running"
}

variable "tier" {
  type        = "string"
  description = "The tier for this service"
}

variable "vpc" {
  type        = "string"
  description = "The target network"
}

variable "zone" {
  type    = "string"
  default = ""
}
