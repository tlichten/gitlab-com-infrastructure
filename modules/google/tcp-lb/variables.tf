variable "lb_count" {}

# list of fqdns for this lb
variable "fqdns" {
  type    = "list"
  default = []
}

# These should be set for an internal load balancer
variable "subnetwork_self_link" {
  default = ""
}

variable "ip_cidr_range" {
  default = "10.0.0.0/18"
}

variable "vpc" {
  default = ""
}

variable "backend_service" {
  default = ""
}

#####################################

variable "external" {
  default = true
}

variable "health_check_ports" {
  type = "list"
}

variable "health_check_request_paths" {
  type = "list"

  default = []
}

variable "forwarding_port_ranges" {
  type = "list"
}

variable "gitlab_zone_id" {}

variable "instances" {
  type = "list"
}

variable "targets" {
  type        = "list"
  description = "target tags for the load balancer"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "name" {}

variable "names" {
  type        = "list"
  description = "Names for the lbs"
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "session_affinity" {
  type        = "string"
  description = "Load balancer session affinity"
  default     = "NONE"
}
