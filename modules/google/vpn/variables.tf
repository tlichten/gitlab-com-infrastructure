variable "name" {
  type        = "string"
  description = "name of the vpn"
  default     = false
}

variable "network_link" {
  type        = "string"
  description = "network link for the vpn"
  default     = false
}

variable "network_name" {
  type        = "string"
  description = "network name for the vpn"
  default     = false
}

variable "region" {
  type        = "string"
  description = "region for the vpn"
  default     = false
}

variable "peer_ip" {
  type        = "string"
  description = "peer ip address for what this is connecting to"
  default     = false
}

variable "shared_secret" {
  type        = "string"
  description = "shared secret of the vpn"
  default     = false
}

variable "dest_subnet" {
  type        = "string"
  description = "destination subnet for the connection"
  default     = false
}

variable "source_subnet" {
  type        = "string"
  description = "source subnet for the connection"
  default     = false
}
