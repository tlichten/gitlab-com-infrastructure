##################################
#
#  Allow internal traffic
#
##################################

resource "google_compute_firewall" "allow-internal" {
  name    = "allow-internal-${var.environment}"
  network = "${google_compute_network.main.self_link}"

  allow {
    protocol = "all"
  }

  source_ranges = ["10.0.0.0/8"]
}

##################################
#
#  Allow azure vpn
#
##################################

resource "google_compute_firewall" "allow-azure-vpn" {
  name    = "allow-vpn-${var.environment}"
  network = "${google_compute_network.main.self_link}"

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443"]
  }

  source_ranges = ["52.177.194.133/32", "52.177.192.239/32"]
}
