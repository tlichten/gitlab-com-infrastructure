resource "google_compute_network" "main" {
  description             = "Main VPC for the ${var.environment} environment"
  name                    = "${var.environment}"
  auto_create_subnetworks = "false"
  project                 = "${var.project}"
}
