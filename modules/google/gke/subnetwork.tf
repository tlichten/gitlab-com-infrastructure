resource "google_compute_subnetwork" "gke" {
  count         = "${var.initial_node_count > 0 ? 1 : 0}"
  name          = "${format("%v-%v", var.name, var.environment)}"
  network       = "${var.vpc}"
  project       = "${var.project}"
  region        = "${var.region}"
  ip_cidr_range = "${var.ip_cidr_range}"
}
