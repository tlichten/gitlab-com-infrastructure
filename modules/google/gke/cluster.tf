resource "google_container_cluster" "cluster" {
  name               = "${format("%v-%v", var.environment, var.dns_zone_name)}"
  zone               = "${data.google_compute_zones.available.names[0]}"
  additional_zones   = ["${data.google_compute_zones.available.names[1]}", "${data.google_compute_zones.available.names[2]}"]
  initial_node_count = "${var.initial_node_count}"
  subnetwork         = "${google_compute_subnetwork.gke.name}"
  min_master_version = "${var.kubernetes_version == "" ? data.google_container_engine_versions.versions.latest_node_version : var.kubernetes_version}"
  node_version       = "${var.kubernetes_version}"
  network            = "${var.vpc}"
  project            = "${var.project}"

  master_auth {
    username = "${var.master_auth_username}"
    password = "${var.master_auth_password}"
  }

  node_config {
    preemptible = "${var.preemptible}"

    machine_type = "${var.machine_type}"
    disk_size_gb = "${var.disk_size}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels {
      environment = "${var.environment}"
    }

    tags = [
      "${var.name}",
      "${var.environment}",
    ]
  }
}
