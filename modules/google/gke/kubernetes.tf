provider "kubernetes" {
  host     = "${google_container_cluster.cluster.endpoint}"
  username = "${var.master_auth_username}"
  password = "${var.master_auth_password}"

  client_certificate     = "${base64decode(google_container_cluster.cluster.master_auth.0.client_certificate)}"
  client_key             = "${base64decode(google_container_cluster.cluster.master_auth.0.client_key)}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.cluster.master_auth.0.cluster_ca_certificate)}"
}

resource "kubernetes_namespace" "inf" {
  metadata {
    name = "inf"
  }
}
