##################################
#
#  Logging for StackDriver
#
##################################

resource "google_logging_project_sink" "log" {
  name = "${var.environment}-logging-sink"

  destination = "storage.googleapis.com/${google_storage_bucket.log.name}"
  filter      = "resource.type = gce_instance"

  # Use a unique writer (creates a unique service account used for writing)
  unique_writer_identity = true
}

resource "google_project_iam_binding" "log" {
  members = ["${google_logging_project_sink.log.writer_identity}"]
  role    = "roles/storage.objectCreator"
}
