# Storage buckets that are common across all environments

variable "environment" {}
variable "versioning" {}
variable "artifact_age" {}
variable "upload_age" {}
variable "lfs_object_age" {}
variable "storage_class" {}
variable "gcs_service_account_email" {}

variable "gcs_storage_analytics_group_email" {}

variable "storage_log_age" {}

resource "google_storage_bucket" "log" {
  name = "gitlab-${var.environment}-logging-archive"

  labels = {
    tfmanaged = "yes"
  }

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"

  lifecycle_rule = {
    action = {
      type = "Delete"
    }

    condition = {
      age = 180
    }
  }
}

resource "google_storage_bucket" "secrets" {
  name = "gitlab-${var.environment}-secrets"

  labels = {
    tfmanaged = "yes"
  }

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"
}

resource "google_storage_bucket" "chef-bootstrap" {
  name = "gitlab-${var.environment}-chef-bootstrap"

  labels = {
    tfmanaged = "yes"
  }

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"
}

resource "google_storage_bucket" "uploads" {
  name = "gitlab-${var.environment}-uploads"

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"

  labels = {
    tfmanaged = "yes"
  }

  lifecycle_rule = {
    action = {
      type = "Delete"
    }

    condition = {
      age     = "${var.upload_age}"
      is_live = "false"
    }
  }

  logging = {
    log_bucket = "gitlab-${var.environment}-storage-logs"
  }
}

resource "google_storage_bucket_iam_binding" "uploads-binding" {
  bucket     = "gitlab-${var.environment}-uploads"
  role       = "roles/storage.objectAdmin"
  depends_on = ["google_storage_bucket.uploads"]

  members = [
    "serviceAccount:${var.gcs_service_account_email}",
  ]
}

resource "google_storage_bucket" "lfs-objects" {
  name = "gitlab-${var.environment}-lfs-objects"

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"

  labels = {
    tfmanaged = "yes"
  }

  lifecycle_rule = {
    action = {
      type = "Delete"
    }

    condition = {
      age     = "${var.lfs_object_age}"
      is_live = "false"
    }
  }

  logging = {
    log_bucket = "gitlab-${var.environment}-storage-logs"
  }
}

resource "google_storage_bucket_iam_binding" "lfs-objects-binding" {
  bucket     = "gitlab-${var.environment}-lfs-objects"
  role       = "roles/storage.objectAdmin"
  depends_on = ["google_storage_bucket.lfs-objects"]

  members = [
    "serviceAccount:${var.gcs_service_account_email}",
  ]
}

resource "google_storage_bucket" "artifacts" {
  name = "gitlab-${var.environment}-artifacts"

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"

  labels = {
    tfmanaged = "yes"
  }

  lifecycle_rule = {
    action = {
      type = "Delete"
    }

    condition = {
      age     = "${var.artifact_age}"
      is_live = "false"
    }
  }

  logging = {
    log_bucket = "gitlab-${var.environment}-storage-logs"
  }
}

resource "google_storage_bucket_iam_binding" "artifacts-binding" {
  bucket     = "gitlab-${var.environment}-artifacts"
  role       = "roles/storage.objectAdmin"
  depends_on = ["google_storage_bucket.artifacts"]

  members = [
    "serviceAccount:${var.gcs_service_account_email}",
  ]
}

resource "google_storage_bucket" "registry" {
  name = "gitlab-${var.environment}-registry"

  versioning = {
    enabled = "${var.versioning}"
  }

  storage_class = "${var.storage_class}"

  labels = {
    tfmanaged = "yes"
  }

  logging = {
    log_bucket = "gitlab-${var.environment}-storage-logs"
  }
}

resource "google_storage_bucket_iam_binding" "registry-binding" {
  bucket     = "gitlab-${var.environment}-registry"
  role       = "roles/storage.objectAdmin"
  depends_on = ["google_storage_bucket.registry"]

  members = [
    "serviceAccount:${var.gcs_service_account_email}",
  ]
}

resource "google_storage_bucket" "storage-logs" {
  name = "gitlab-${var.environment}-storage-logs"

  storage_class = "${var.storage_class}"

  lifecycle_rule = {
    action = {
      type = "Delete"
    }

    condition = {
      age     = "${var.storage_log_age}"
      is_live = "true"
    }
  }

  labels = {
    tfmanaged = "yes"
  }
}

resource "google_storage_bucket_iam_binding" "storage-analytics" {
  bucket     = "gitlab-${var.environment}-storage-logs"
  role       = "roles/storage.legacyBucketWriter"
  depends_on = ["google_storage_bucket.storage-logs"]

  members = [
    "group:${var.gcs_storage_analytics_group_email}",
  ]
}
