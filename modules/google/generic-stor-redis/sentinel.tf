resource "google_compute_address" "sentinel-static-ip-address" {
  count        = "${var.sentinel_count}"
  name         = "${format("%v-sentinel-%02d-%v-%v-static-ip", var.name, count.index + 1 + 120, var.tier, var.environment)}"
  address_type = "INTERNAL"
  address      = "${replace(var.ip_cidr_range, "/\\d+\\/\\d+$/", count.index + 1 + 120)}"
  subnetwork   = "${google_compute_subnetwork.subnetwork.self_link}"
}

resource "google_compute_disk" "sentinel_data_disk" {
  project = "${var.project}"
  count   = "${var.sentinel_count}"
  name    = "${format("%v-sentinel-%02d-%v-%v-data", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.sentinel_data_disk_size}"
  type    = "${var.sentinel_data_disk_type}"

  labels {
    environment  = "${var.environment}"
    pet_name     = "${var.name}"
    do_snapshots = "true"
  }

  lifecycle {
    ignore_changes = ["snapshot"]
  }
}

resource "google_compute_disk" "sentinel_log_disk" {
  project = "${var.project}"
  count   = "${var.sentinel_count}"
  name    = "${format("%v-sentinel-%02d-%v-%v-log", var.name, count.index + 1, var.tier, var.environment)}"
  zone    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"
  size    = "${var.log_disk_size}"
  type    = "${var.log_disk_type}"

  labels {
    environment = "${var.environment}"
  }
}

resource "google_compute_instance" "sentinel_instance_with_attached_disk" {
  count        = "${var.sentinel_count}"
  name         = "${format("%v-sentinel-%02d-%v-%v", var.name, count.index + 1, var.tier, var.environment)}"
  machine_type = "${var.sentinel_machine_type}"

  metadata = {
    "CHEF_URL"                = "${var.chef_provision.["server_url"]}"
    "CHEF_VERSION"            = "${var.chef_provision.["version"]}"
    "CHEF_NODE_NAME"          = "${var.use_new_node_name ? format("%v-sentinel-%02d-%v-%v.c.%v.internal", var.name, count.index + 1, var.tier, var.environment, var.project) : format("%v-sentinel-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)}"
    "GL_KERNEL_VERSION"       = "${var.kernel_version}"
    "CHEF_ENVIRONMENT"        = "${var.environment}"
    "CHEF_RUN_LIST"           = "${var.sentinel_chef_run_list}"
    "CHEF_DNS_ZONE_NAME"      = "${var.dns_zone_name}"
    "CHEF_PROJECT"            = "${var.project}"
    "CHEF_BOOTSTRAP_BUCKET"   = "${var.chef_provision.["bootstrap_bucket"]}"
    "CHEF_BOOTSTRAP_KEYRING"  = "${var.chef_provision.["bootstrap_keyring"]}"
    "CHEF_BOOTSTRAP_KEY"      = "${var.chef_provision.["bootstrap_key"]}"
    "CHEF_INIT_RUN_LIST"      = "${var.chef_init_run_list}"
    "GL_PERSISTENT_DISK_PATH" = "${var.persistent_disk_path}"
    "GL_FORMAT_DATA_DISK"     = "${var.format_data_disk}"
    "block-project-ssh-keys"  = "${var.block_project_ssh_keys}"
    "enable-oslogin"          = "${var.enable_oslogin}"
    "shutdown-script"         = "${file("${path.module}/../../../scripts/google/teardown-v1.sh")}"
  }

  metadata_startup_script = "${file("${path.module}/../../../scripts/google/bootstrap-v${var.bootstrap_version}.sh")}"
  project                 = "${var.project}"
  zone                    = "${var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)}"

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = "${var.service_account_email}"

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = "${var.preemptible}"
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = "${var.os_boot_image}"
      size  = "${var.os_disk_size}"
      type  = "${var.os_disk_type}"
    }
  }

  attached_disk {
    source = "${google_compute_disk.sentinel_data_disk.*.self_link[count.index]}"
  }

  attached_disk {
    source      = "${google_compute_disk.sentinel_log_disk.*.self_link[count.index]}"
    device_name = "log"
  }

  network_interface {
    subnetwork    = "${google_compute_subnetwork.subnetwork.name}"
    address       = "${google_compute_address.sentinel-static-ip-address.*.address[count.index]}"
    access_config = {}
  }

  labels {
    environment = "${var.environment}"
    pet_name    = "${var.name}"
  }

  tags = [
    "${var.name}",
    "${var.environment}",
  ]

  provisioner "local-exec" {
    when    = "destroy"
    command = "knife node delete ${format("%v-sentinel-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)} -y; knife client delete ${format("%v-sentinel-%02d.%v.%v.%v", var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name)} -y; exit 0"
  }
}
