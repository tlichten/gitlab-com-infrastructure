output "instances_self_link" {
  value = "${google_compute_instance.instance_with_attached_disk.*.self_link}"
}
