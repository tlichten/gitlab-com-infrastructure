resource "google_compute_firewall" "single" {
  # egress support is not available yet  # https://github.com/terraform-providers/terraform-provider-google/pull/306

  name    = "${var.name}-firewall"
  network = "default"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "udp"
  }

  allow {
    protocol = "tcp"
  }

  source_ranges = ["${var.vpn_ips[0]}/32", "${var.vpn_ips[1]}/32", "${var.prod_ip}/32", "${var.prometheus_ip}/32"]
  target_tags   = ["${var.name}"]
}
