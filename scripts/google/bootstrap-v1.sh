#!/bin/bash

exec &> >(tee -a "/tmp/bootstrap.log")

#Pass env variables
for i in $(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/" -H "Metadata-Flavor: Google"); do
  if [[ $i == CHEF* ]] ;
  then
    export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
  fi
done

#Lookup consul's service endpoint
apt-get install jq -y -q

# TODO
# CONSUL_IP=$(gcloud compute --project=${CHEF_PROJECT} forwarding-rules list --filter="description~inf/${CHEF_ENVIRONMENT}-${CHEF_DNS_ZONE_NAME}-consul-serf" --format="json"|jq ".[0].IPAddress")

# Format persistent disk
if [[ -L /dev/disk/by-id/google-persistent-disk-1 ]]; then
    mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb
    mkdir -p /var/opt/gitlab/
    mount -o discard,defaults /dev/sdb /var/opt/gitlab
    echo UUID="$(sudo blkid -s UUID -o value /dev/sdb)" /var/opt/gitlab ext4 discard,defaults 0 2 | tee -a /etc/fstab
fi

# Install chef

curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v "${CHEF_VERSION}"
mkdir /etc/chef

# Get validation.pem from gkms
gsutil cp gs://gitlab-gprd-chef-boostrap/validation.enc /tmp/validation.enc

gcloud kms decrypt --keyring=gitlab-gprd-bootstrap --location=global --key=gitlab-gprd-bootstrap-validation --plaintext-file=/etc/chef/validation.pem  --ciphertext-file=/tmp/validation.enc


rm -f /tmp/validator.enc

# create client.rb
cat > /etc/chef/client.rb <<-EOF
chef_server_url  "$CHEF_URL"
validation_client_name "gitlab-validator"
log_location   STDOUT
node_name "$CHEF_NODE_NAME"
environment "$CHEF_ENVIRONMENT"
EOF

# create run_list
cat > /etc/chef/first-run.json <<-EOF
{
  "run_list":[ $CHEF_RUN_LIST ]
}
EOF

# run chef
chef-client -j /etc/chef/first-run.json
