#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script is passed as a startup-script to GCP instances
###################################################
###    NOTE: It is being run on _every_ boot    ###
###  It MUST be non destructive and itempotent  ###
###################################################

exec &> >(tee -a "/tmp/bootstrap.log")
set -x
env

# Pass env variables
for i in $(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/" -H "Metadata-Flavor: Google"); do
    if [[ $i == CHEF* ]]; then
        export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
    fi
    if [[ $i == GL* ]]; then
        export "$i"="$(curl -s "http://metadata.google.internal/computeMetadata/v1/instance/attributes/$i" -H "Metadata-Flavor: Google")"
    fi
done

# Lookup consul's service endpoint
apt-get install jq -y -q

format_ext4() {
    mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard $1
}

mount_device() {
    local device_path=$1
    local mount_path=$2

    mkdir -p "$mount_path"
    if ! grep -qs "$mount_path" /proc/mounts; then
        mount -o discard,defaults $device_path "$mount_path"
    fi
    local UUID="$(sudo blkid -s UUID -o value $device_path)"
    if ! grep -qs "$UUID" /etc/fstab; then
        echo UUID="$UUID" "$mount_path" ext4 discard,defaults 0 2 | tee -a /etc/fstab
    fi
}

if [[ -L /dev/disk/by-id/google-log ]]; then
    if [[ $(file -sL /dev/disk/by-id/google-log) != *Linux* ]]; then
      format_ext4 /dev/disk/by-id/google-log
    fi

    # In case we resized the underlying GCP disk
    resize2fs /dev/disk/by-id/google-log

    mount_device /dev/disk/by-id/google-log /var/log
fi

# default to false, force a reformat even if there is an existing
# Linux filesystem
GL_FORMAT_DATA_DISK=${GL_FORMAT_DATA_DISK:-false}

if [[ -b /dev/sdb && ("true" ==  "${GL_FORMAT_DATA_DISK}" || $(file -sL /dev/sdb) != *Linux*) ]]; then
    format_ext4 /dev/sdb
fi

# Proceed with mounting
if [[ -L /dev/disk/by-id/google-persistent-disk-1 ]]; then
    mount_device /dev/sdb "${GL_PERSISTENT_DISK_PATH:-/var/opt/gitlab}"
fi

# Install chef

curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v "${CHEF_VERSION}"
mkdir /etc/chef

# Get validation.pem from gkms
gsutil cp gs://$CHEF_BOOTSTRAP_BUCKET/validation.enc /tmp/validation.enc

gcloud kms decrypt --keyring=$CHEF_BOOTSTRAP_KEYRING --location=global --key=$CHEF_BOOTSTRAP_KEY --plaintext-file=/etc/chef/validation.pem  --ciphertext-file=/tmp/validation.enc

if [[ ! -e /var/lock/inital-boot-run.lock ]]; then
  export CHEF_CLIENT_ARGS="-o $(echo "$CHEF_RUN_LIST,$CHEF_INIT_RUN_LIST" | sed 's/"\|,$//g')"
fi

rm -f /tmp/validator.enc

# create client.rb
cat > /etc/chef/client.rb <<-EOF
chef_server_url  "$CHEF_URL"
validation_client_name "gitlab-validator"
log_location   STDOUT
node_name "$CHEF_NODE_NAME"
environment "$CHEF_ENVIRONMENT"
EOF

# run chef
chef-client $CHEF_CLIENT_ARGS
# persist the run list
knife node -c /etc/chef/client.rb run_list set $CHEF_NODE_NAME $(echo $CHEF_RUN_LIST | tr -d '"')

touch /var/lock/inital-boot-run.lock